+++
title = "Thoughts on A910"
aliases = ["2008/06/thoughts-on-a910.html"]
date = "2008-06-05T15:50:00Z"
[taxonomies]
categories = ["projects"]
tags = ["A910", "development", "EZX/LJ", "ezx_flexbit.cfg", "OpenEZX"]
authors = ["peter"]
+++
I really like my A910, I use it everyday and I believe it is a great phone - especially for it's low price (used devices, but hey, that does not really matter).
Since the WiFi is working, it's even better.
<!-- more -->
After having stopped to work on A910s firmware around a month ago, I will now go on with that again, as there are still some things to do.

Some fixes will be just done to improve the phone a little bit, e.g. I will clean up some path related things and use a newer version of busybox - there are some other actualization's on my imaginary list. Additionally, i think of other thinks like flexbit editing. I will try whether it's possible to use the writable memory as a place for ezx_flexbit.cfg - would be great to be able to edit it without re-flashing all the time, to figure out which byte does what.

For further development there are plans like compiling a new kernel (based on the published kernel sources) with additional modules and I still dream of a real task manager for the A910.

But one thing is for sure: As far as there is a way to run the phone with a 2.6.* kernel with working phone etc. (does not matter whether Openmoko, QTopia or Android), I will immediately quit any development on the good old Motorola Software ;)
