+++
title = "Purism Librem 5 Announced, Crowdfunding Started"
aliases = ["2017/08/25/librem-5-announced-crowdfunding-started.html"]
author = "Peter"
date = "2017-08-25T09:11:54Z"
layout = "post"
[taxonomies]
tags = ["developer phones", "GNU/Linux", "opensource", "Purism", "Librem 5"]
categories = ["shortform"]
authors = ["peter"]
+++

Purism has now launched their [campaign to crowdfund their the Librem 5
smartphone](https://puri.sm/shop/librem-5/) I was writing about
[recently](https://linmob.net/2017/08/19/purism-follows-openmoko/).<!-- more -->  

They ask for USD 1.5 Mio in total. Perks include a developer kit at USD 299
scheduled for June 2018, and (of course) the Librem 5 priced at USD 599,
scheduled for January 2019.  

The campaign will continue for 59 days, which (if I am not mistaken) makes
October 23rd the last day of their campaign.  

_Note that the Crowdfunding happens on their own platform, they are not using
Kickstarter or Indiegogo._

