+++
title = "The Nook Glowlight 3"
aliases = ["2018/11/17/the-nook-glowlight-3.html"]
author = "Peter"
date = "2018-11-17T12:12:44Z"
layout = "post"
[taxonomies]
tags = ["E-Reader", "eBooks", "eInk", "eInk tablets", "FDroid", "Nook"]
categories = ["impressions"]
authors = ["peter"]
+++
_Yet another post on a device featuring an eInk screen, how likely is that? I honestly don't know, but this is another post about such a thing._

I have been an avid buyer of the devices US book giant Barnes &amp; Noble has made under the Nook brand since 2012, when I purchased a Nook Simple Touch. <!-- more -->Then, in 2015 (?) I upgraded to a Nook Glowlight Plus which I unfortunately broke a couple weeks ago. Recently, I purchased the latest iteration of Barnes &amp; Noble's 6" eInk product: The Nook Glowlight 3.



### Hardware or "Design is how it feels"

Where the Glowlight Plus was a water proofed device (IP 67) with one capacitive button and an metal back, the Glowlight 3 reminds me a lot of the Nook Simple Touch which I adored for its handy design: It just was a pleasure to hold and use. The Glowlight 3 is a lot like its great-grandparent. To me, it does not feel quite as nice, as the back isn't as contoured and feels less rubbery. But, compared to the cold, heavy metal feeling of the Glowlight Plus, it feels really nice to hold, which in part is also due to the huge bezels.[^1]  While the recessed, optical/infrared touch screen feels like a downgrade to the capacitive, flush touchscreen of its predecessor, it – like the big bezels – helps with avoiding touching the screen accidentally. Also, you don't need to use that touch screen that often while reading: The Nook Glowlight 3 after two generation reintroduces the page turn buttons last seen on the Nook Simple Touch with Glowlight. Minor nitpick: The page turn buttons are too loud for my liking, so much so I would not want to use them in a silent environment.[^2]

Technically, aside from the aforementioned differences, the only truly new feature of the Glowlight 3 is the "Night light". It's quite a simple thing: The frontlight can turn red-ish, which is easier on the eyes. It's just nice. Really nice, so much so, that it – if you are, like me, into Nook e-readers, and you read a lot at night in the dark, it should make the upgrade worthwhile on its own merits.

### Software

I live in Europe. So why do I even bother with an e-reader that is so much made for the United States, that you can't even set a non-American time zone during the initial setup?

Simple! It runs Android 4.4.2 (KitKat) which – to my knowledge – is the latest iteration of Android you can get on e-readers at this price point.[^3] That makes it very tinker friendly, and introduces you to a ton of options – although the eInk screen and the single core i.MX6 CPU and the meager 512 MB of Ram are severely limiting. So just forget using all these overly animated or memory intensive apps on this device – these won't be pleasant.


So which apps am I using? I run the <a href="https://fdroid.org">FDroid store</a> and also the Yalp store[^4], use the KISS Launcher[^5] and for reading I have special eInk optimized versions of CoolReader and FBReader I downloaded years ago from xda-developers or mobiread forums, and also the Android port of <a href="https://koreader.rocks/">KOReader</a>. And then there is <a href="https://f-droid.org/de/packages/org.sufficientlysecure.viewer/">DocumentReader</a>. Reading with multiple apps comes in handy, because I usually read (or re-read) multiple books at a time. As changing books in all these apps is often rather clunky, I love the fact that I just have to hit the home button to get back to the launcher and can then choose a different app to change to another book.

_Buttons._ That's a big one. The Nook is my only device a run the XPosed framework on, as it in conjunction with the <a href="https://bitbucket.org/Ryogo/n-toolkit/downloads/">N+ Toolkit</a> allows to simply customize that home button to do more than it does by default. I also made it so (by editing a couple system files), that I can use the page turn buttons on the left side of my Nook Glowlight 3 to function in those reading apps I run on the device.[^6]

_Now why all this hassle?_ Good question. While I believe, that except for the night light-feature of the Nook the Kindle Voyage I also own is the better device, as it is far smoother in daily operation and the better choice for most people, I like to have an eReader that can be a tad more versatile and can be extended in functionality. I would like it to be able to do more than it can, as the Nook Glowlight 3 unfortunately has neither USB Host capability nor Bluetooth, scenarios like attaching a keyboard and using this Android eReader as an ePaper typewriter are not attainable. So functionality can't be it. In fact, I think that I like the fact that with the modified software I have an eReader that offers me some privacy. There is no cloud sync that would tell some server on which page of an ebook I am or which passages I marked or commented on.

### Conclusion

Should you get one? Unless you are as weird as I am, I don't think so. If you want a basic eReader, get one that integrates well with your preferred book store. If you want a versatile eInk device, you should be better of with one of <a href="https://www.aliexpress.com/store/3333002">Boyues offerings</a>, that are more open by default or the Dasung <a href="https://www.indiegogo.com/projects/not-ereader-first-e-ink-mobile-phone-monitor#/">"Not-eReader"</a>, which seems to be an interesting beast.


[^1]: Which I don't find too aesthetically displeasing, although the fact that chin and forehead are more massive than the side bezels feels unnecessary for handling. Also, that nook logo on the devices forehead feels slightly misplaced.

[^2]: Which isn't a problem at all, as touching or swiping the screen to turn pages works just fine.

[^3]: For perspective: Tolino, a german e-reader brand makes e-readers that still run Android 4.0.4 (Ice Cream Sandwich); Boyue, a chinese e-reader maker offers Android 4.2.2 (Jelly Bean).

[^4]: A FOSS app that can download free and (when logged in with a personal Google-Account) paid apps.

[^5]: I should probably try other light launchers out, but I am just too lazy, sorry.

[^6]: When I hold the device in my right hand, I can just tap the screen to easily flip to the next page.
