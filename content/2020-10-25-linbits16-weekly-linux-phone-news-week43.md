+++
title = "LinBits 16: Weekly Linux Phone news / media roundup (week 43)"
aliases = ["2020/10/25/linbits16-weekly-linux-phone-news-week43.html", "linbits16"]
author = "Peter"
date = "2020-10-25T14:43:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "Librem 5", "Purism", "PlasmaMobile", "Squeekboard", "Tor", "Arch Linux ARM", "Manjaro", "postmarketOS", "Linux", "Ubuntu Touch", "LINMOBapps", "SailfishOS"]
categories = ["weekly update"]
authors = ["peter"]
+++

_It's sunday. Now what happened since last sunday?_

Squeekboard 1.10.0, postmarketOS has a new kernel, Raspberry Pi 4 Compute Modules and more. _Commentary in italics._
<!-- more -->

### Software releases and improvements
* [Squeekboard 1.10.0](https://source.puri.sm/Librem5/squeekboard/-/commit/c0525946ae799d269d966ad5854c87c517dc52a7) brings (among other features) Xwayland support which is going to help with apps that have not fully embraced Wayland like Glimpse, Chromium or Anbox.
* [postmarketOS have upgraded their Linux kernel](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/1614) on the PinePhone to megi's 5.9, which is great news as it should bring many improvements.
* [DanctNIX Mobile/Arch Linux ARM](https://github.com/dreemurrs-embedded/Pine64-Arch/releases) has seen a new release. New features include scrolling fixes, the new Squeekboard release and more. Existing users need to add another repo: 
Just edit `/etc/pacman.conf` and add the following as you can see in this [photo](https://twitter.com/linmobblog/status/1319948249424449536):
~~~~~
[danctnix]
SigLevel = Never
Server = https://p64.arikawa-hi.me/danctnix/aarch64/
~~~~~

### Hardware
* [Raspberry Pi 4 Compute Module](https://www.raspberrypi.org/products/compute-module-4/?variant=raspberry-pi-cm4001000&resellerType=home). _If you think that PinePhone and Librem 5 are too slow and want to build your own Linux Phone, this would be a good platform I think. Remember to plan for cooling and a massive battery though._ 

### Worth reading
* Mudita: [MuditaOS goes Open Source - join the Developer Preview!](https://mudita.com/community/blog/muditaos-goes-open-source-join-the-developer-preview/). _Mudita is going to make old style candybar phone with an eInk display, which is certainly going to have better battery life than PinePhone or Librem 5. I am not too sure what to make of this, but it might be interesting, right?_
* Phoronix: [Linux 5.10 Has Initial Support For NVIDIA Orin, DeviceTree For Purism's Librem 5](https://www.phoronix.com/scan.php?page=news_item&px=Linux-5.10-ARM-Platform). _Librem 5 owners and backers rejoice!_
* LinuxSmartphones: [PostmarketOS update brings HDMI support for the PinePhone and PineTab](https://linuxsmartphones.com/postmarketos-update-brings-hdmi-support-for-the-pinephone-and-pinetab/). _This is that new kernel, see above._
* LinuxSmartphones: [](https://linuxsmartphones.com/first-look-manjaro-arm-beta-1-with-phosh-on-the-pinephone/). _This is a great review of Manjaro Beta 1, although I do have some nitpicks with regard to mentioning proper application names. But that's just me._
* Gamey: [Best Matrix clients for the Pinephone & Librem5 ArchARM Phosh](https://lbry.tv/@gamey:c/Matrix-on-Pinephone:3). _My current favourite is Mirage and I am curious how [NeoChat](https://invent.kde.org/network/neochat/), a Kirigami2-fork of Spectral is going to develop._
* Purism: [Specify Form-Factors in Your Librem 5 Apps](https://puri.sm/posts/specify-form-factors-in-your-librem-5-apps/). _I like this! I hope that PinePhone distributions will adopt this too to make their App Stores more user friendly._
* TuxPhones: [⚡ Ubuntu Touch is coming to the Google Pixel 3a](https://tuxphones.com/ubuntu-touch-linux-google-pixel-3a/). _Keep in mind that the awesome Pixel Camera is mostly achieved through software, which won't be available on Ubuntu Touch._

### Worth listening

* Linux Unplugged 376: [From the Factory Floor](https://linuxunplugged.com/376). _At 21:02 there's talk about the PinePhone Manjaro Community Edition, which is going to ship with Phosh._

### Worth watching
* Avisando: [PinePhone - Official keyboard ](https://www.youtube.com/watch?v=qeN2kaCEfeE). _What the title says._
* Purism: [A Librem 5 Video Made on a Librem 5](https://www.youtube.com/watch?v=8WDN2nEv4JQ). _This is impressive._
* PINE64: [PinePhone Retro Gaming](https://www.youtube.com/watch?v=yILXtmc7Wrc). _Retro gaming is quite cool, and it's nice that they shared their configuration here._
 * UBports: [Ubuntu Touch Q&A 87](https://www.youtube.com/watch?v=boSyFN8EuAY). _OTA 14 merge window closes. Fixes include Bluetooth audio playback will continue using Bluetooth after dis- and reconnecting. New devices: Z4 Tablet and Nexus 6P. Google Pixel 3a and Pixel 3a XL are likely going to be in the UBports installer soon. Also several apps need a new maintainer, so if you want to take responsibility, please help out! The app store discussion is pretty informative too. There is much more in this one, my time budget was so poor that I could not pay full attention. ;-)_
 * Privacy and Security Tips: [Howto: SSH Hidden Service .Onion Only Access On Pinephone (Mobian) (Security Enhancement: Any Linux)](https://www.youtube.com/watch?v=syKZNMVxTM4). _For the privacy minded._
 * Liliputing/LInux Smartphones: [Manjaro Beta1 with Phosh on the PinePhone](https://www.youtube.com/watch?v=myNqL9A_YV8). _See above for the article._
 * HomebrewXS: [How to install Plasma Mobile on PinePhone (Easy way)](https://www.youtube.com/watch?v=UFem-QJCwYg). _Make sure to check out my Plasma Mobile video that is linked below, too._
 * Elatronion: [This Phone Doesn't Spy On You: Pinephone overview](https://lbry.tv/@Elatronion:a/pinephone:3). _Great video!_

### Stuff I did

#### Content
I shot and published two videos:
* one about the "[current state of Plasma Mobile](https://www.youtube.com/watch?v=Pz9DgFRmOMQ)",
* and one about [DanctNIX Mobile and LINMOBapps](https://www.youtube.com/watch?v=dxup2c9aNzE) (which had real bad timing as a new release came out between photography and publication).

I have also decided that I am going to provide a __mirror of all my videos on a VPS__ for people who (understandibly) don't like YouTube. I am not sure how to implement this, currently I am thinking of adding blog posts with the video description and embedding the videos there, providing a more complete feed that then is only going to lack my social media activity on Twitter and Fosstodon. This move will also most likely mean that I won't upload any more videos to #PeerTube, mainly because it's too time consuming given that it's one more place to upload stuff to and where I need to keep track of comments. 
As with previous announcements I made in this section of the "Weekly Update": Don't expect this to happen immidiately.
If you have thoughts or recommendations (e.g. on video hosting or on including Fosstodon or Twitter into an RSS feed), please get in touch!

#### LINMOBapps
* LINMOBapps has now more than 200 apps. I still did not branch out games (and I intend to maintain a copy of the complete list of apps and games around to allow contributions back to MGLapps), but I am going to do that soon. If you want to help or have helpful suggestions, please get in touch!

#### and more ...
* I played a bit with Sailfish X on the SONY Xperia X, and it has some pretty cool apps that I would love to see ported to other Linux on Mobile distributions.
