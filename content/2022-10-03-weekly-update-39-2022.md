+++
title = "Weekly GNU-like Mobile Linux Update (39/2022): Plasma Mobile Gear 22.09, Akademy and an Update on Nemo Mobile"
date = "2022-10-03T20:00:00Z"
updated = "2022-10-04T11:00:00Z"
draft = false
[taxonomies]
tags = ["PinePhone Keyboard", "Plasma Mobile", "Akademy", "GNOME OS", "Capyloon", "Nemo Mobile",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = "with contributions by Niko, hamblinggreen and with friendly assistance from plata's awesome script)"
+++
This iteration of the Weekly Update contains less headlines than usually. 
<!-- more -->
_Commentary in italics._

### Hardware
* [Fydetab Duo - First consumer-grade Chromium OS tablet](https://fydetabduo.com/). _Chrome OS, without Google._
   * [We have made something for you: Introducing Fydetab Duo - YouTube](https://www.youtube.com/watch?v=X5lihbVbNQA)
   
### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#63 Experiments and Prototypes](https://thisweek.gnome.org/posts/2022/09/twig-63/). _Good news about News Flash, Komikku and Fractal, and look into Workbench if you develop apps!_
- Julian Sparber: [Fractal security audit](https://blogs.gnome.org/jsparber/2022/09/27/fractal-security-audit/)
- Phoronix: [System76's Pop!\_OS COSMIC Desktop To Make Use Of Iced Rust Toolkit Rather Than GTK](https://www.phoronix.com/news/COSMIC-Desktop-Iced-Toolkit)

##### GNOME OS
- Gnome OS test images now being built for
    - [Pinephone Pro](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/1750)
    - [Pinephone](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/1727)
    - [Original issue](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/issues/338)

#### Plasma/Maui ecosystem
- Plasma Mobile: [Plasma Mobile Gear ⚙ 22.09 is Out](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/). _5.26 is going to be great on mobile, and that mail app looks promising!_
- Nate Graham: [No blog post this week](https://pointieststick.com/2022/10/01/no-blog-post-this-week/)
- Nate Graham: [Making it easier to submit bug reports](https://pointieststick.com/2022/09/28/making-it-easier-to-submit-bug-reports/)
- Volker Krause: [August/September in KDE Itinerary](https://www.volkerkrause.eu/2022/09/30/kde-itinerary-august-september-2022.html)
- jriddell.org: [Akademy 2022 in Barcelona Day 1 Talks](https://jriddell.org/2022/10/02/akademy-2022-in-barcelona-day-1-talks/)
- Qt blog: [Dialogs in Qt Quick](https://www.qt.io/blog/dialogs-in-qt-quick)

#### Nemo Mobile
- Nemo Mobile UX team: [Nemomobile in September 2022](https://nemomobile.net/pages/nemomobile-in-september-2022/) *Nemo Mobile is looking for a postmarketOS maintainer.*

#### Capyloon
- Capyloon: [Tue 27 September 2022](https://capyloon.org/releases.html#sept-27)

#### Ubuntu Touch 
- Lemmy - linuxphones: [early-stage bootable version of Ubuntu Touch based on 20.04 available](https://lemmy.ml/post/503528)
- Lemmy - linuxphones: [Fairphone 4 Ubuntu Touch (official port)](https://lemmy.ml/post/503535)

#### Distributions
- Kupfer Blog: [v0.1.3: Two For One](https://kupfer.gitlab.io/blog/2022-10-02_v0-1-3_two_for_one.html)
- Manjaro Blog: [September 2022 in Manjaro ARM](http://blog.manjaro.org/2022/09/27/september-2022-in-manjaro-arm/)
- Phoronix: [Debian Chooses A Reasonable, Common Sense Solution To Dealing With Non-Free Firmware](https://www.phoronix.com/news/Debian-Non-Free-Firmware-Result)
- postmarketOS wiki:[Gnome Mobile Shell is now documented!](https://wiki.postmarketos.org/wiki/Gnome_Shell_for_Mobile)

#### Kernel
- Phoronix: [Rust Infrastructure Pull Request Submitted For Linux 6.1!](https://www.phoronix.com/news/Rust-For-Linux-6.1-Pull-Request)
- Phoronix: [PinePhone Keyboard Driver Prepped Ahead Of Linux 6.1](https://www.phoronix.com/news/PinePhone-Keyboard-Linux-6.1)

#### Stack
- Phoronix: [Blumenkrantz Flushes 17.1k Lines Of Old Mesa Code](https://www.phoronix.com/news/Blumenkrantz-Nukes-17.1k-Mesa)

#### Matrix
- nheko (Matrix): [v0.10.2](https://github.com/Nheko-Reborn/nheko/releases/tag/v0.10.2)
- matrix.org: [This Week in Matrix 2022-09-30](https://matrix.org/blog/2022/09/30/this-week-in-matrix-2022-09-30)
- matrix.org: [Matrix v1.4 release](https://matrix.org/blog/2022/09/29/matrix-v-1-4-release)
- matrix.org: [Upgrade now to address E2EE vulnerabilities in matrix-js-sdk, matrix-ios-sdk and matrix-android-sdk2](https://matrix.org/blog/2022/09/28/upgrade-now-to-address-encryption-vulns-in-matrix-sdks-and-clients)


### Worth noting
- Purism forums: [Firefox ESR && flickering Bookmarks](https://forums.puri.sm/t/firefox-esr-flickering-bookmarks/18347). _The great thing about this issue is that anyone familiar with web technologies can help fixing this [upstream](https://gitlab.com/postmarketOS/mobile-config-firefox)!_
* u/BubblyKombucha: [Pro Tip: Waydroid with Google Play Services](https://www.reddit.com/r/pinephone/comments/xqleo6/pro_tip_waydroid_with_google_play_services/). _I don't want Play Services anywhere, but if you do, that's fine and this might be helpful!_
* @linuxphoneapps: ["400. FINALLY! https://linuxphoneapps.org/apps/"](https://twitter.com/linuxphoneapps/status/1575850609953030144#m)

### Worth reading
- Martijn Braam: [Automated Phone Testing pt.3](https://blog.brixit.nl/automated-phone-testing-pt-3/)
- Chris Davis: [Progress Update For GNOME 43](https://blogs.gnome.org/christopherdavis/2022/09/29/progress-update-for-gnome-43/)
- TheEvilSkeleton: [What Not to Recommend to Flatpak Users](https://theevilskeleton.gitlab.io/2022/09/28/what-not-to-recommend-to-flatpak-users.html)
- Lup Yuen Lee: [Understanding PinePhone's Display (MIPI DSI)](https://lupyuen.github.io/articles/dsi)
- Norman Feske for Genodians: [Pine fun - How low can we go...](https://genodians.org/nfeske/2022-09-29-pine-fun-how-low-can-we-go). _If you're interested in battery life, you must read this!_
- Purism: [Reclaiming Digital Privacy](https://puri.sm/posts/reclaiming-digital-privacy/)


### Worth watching
- Bhushan Shah & Devin Lin at Akademy 2022: [Plasma Mobile in 2022](https://tube.kockatoo.org/w/1Yd26fiAAhmrZnVvGcoC86?start=4h56m41s). _I hope I've picked a reasonable timestamp..._ [Talk details](https://conf.kde.org/event/4/contributions/96/).
- Phalio: [PinePhone Keyboard Typing](https://tube.tchncs.de/w/fEvGjTZ71cBUN9D9Kw61Ju)
- not robot: [postmarketos pocof1](https://www.youtube.com/watch?v=EHSQ17qfrUo). _GNOME mobile for the win!_
- /u/oklopfer: [Mobile Linux: It’s time for Android to be Scared (PinePhone Pro + Mobian + GNOME + Waydroid)](https://www.reddit.com/r/linux/comments/xptx2x/mobile_linux_its_time_for_android_to_be_scared/). _A Waydroid demo (on GNOME Shell without mobile patches). Mental Health Advisory: Don't read the comments, it's r/linux._
- akrosi8: [Xonotic on the PinePhone](https://www.youtube.com/watch?v=Pq7pKTWi_kI)
- Aji2205: [Install linux debian on wm8650 old tablet](https://www.youtube.com/watch?v=y2o5I8MRLRU)
- Jhony Kernel: [neofetch postmarketOs nokia n900](https://www.youtube.com/watch?v=F4XAvTOG1e0)
- (RTP) Privacy Tech Tips: [Set Up Tor ❄️ Snowflake Proxy On Any Linux Device](https://www.youtube.com/watch?v=TRvt6CCi_VY)
- Purism: [Reclaim Your Privacy Online](https://www.youtube.com/watch?v=kj4Xoj3gWLI)
- Dev Café: [Exécuter une application Flutter sur un téléphone Linux (postmarketOS) 👨‍💻 Tutoriel Dart / Flutter](https://www.youtube.com/watch?v=NkbGpUgFO_g). _French, but quite interesting!_
- David Bombal: [Is it possible to hack WiFi with a phone?](https://www.youtube.com/watch?v=1Kg0Eh7n-RM).[^1] _Spoiler: Yes. Warning: Python 2 required, apparently._

### Thanks

Huge thanks to  

- niko (who contributed to the last few weeks too btw :))
- [hamblingreen,](https://hamblingreen.gitlab.io) *currently dabbling in self-hosting*

and possible further anonymous contributors for helping with this weeks update - and to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot. 

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email - or just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)
PPS: This one has less headlines - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update%2039&body=Less%20or%20more%20headlines%20going%20forward%3F)

[^1]: This video was added after initial publication on October 4th, 2022.
