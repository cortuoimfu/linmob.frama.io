+++
title = "Attending FOSDEM 2023 for fun and profit"
date = "2023-02-12T20:20:00Z"
draft = false
[taxonomies]
tags = ["FOSDEM 2023", "travel", "camera", "Librem 5", "postmarketOS", "Mobian", "Ubuntu Touch"]
categories = ["personal", "events",]
authors = ["peter"]
[extra]
edit_post_with_images = true
+++

On February 4th and 5th 2023, I attended FOSDEM for the first time. It was a blast, and this is a report on how it went.
<!-- more -->

### Day 0: Getting there

Since I don't like airports and air travel, I decided to travel by train, a journey of 6 hours and 45 minutes. Booking really early made my ticket super affordable, so affordable that I opted to travel first class.[^1] This went well, even with mobile linux. DB has a check-in page served via their on-train WiFi which allows you to check in. Worked just fine on my Surface Go 2 on the first train, and it also worked on my Librem 5 on the way back. [^2] Unfortunately, I did not make the first connection and thus arrived a bit over two hours late (21:46 instead of 19:35), which led me to just check in at the hotel (right next to Bruxelles Midi) and not go to any of the social events that happened on Friday.

### Day 1: Saturday

#### FOSS on Mobile dev room

While I woke up earlier than I wanted to, I still managed to be slow to get going, and when I arrived, I just went straight to the room UB4.136, which then still was quite empty, and for a few minutes after my arrival, I would be just alone in it. It would not stay that way.

To shorten this post, I've added [comments to my post that detailed talks to watch](https://linmob.net/looking-forward-to-fosdem/) - read them now, if you are interested in my thoughts. 

All talks were great, with [Alfred's talk about Lomiri](https://fosdem.org/2023/schedule/event/lomiri/) and [Norman's talk about Genode on the PinePhone](https://fosdem.org/2023/schedule/event/genode_on_the_pinephone/) standing out to me, likely because both were touching subjects I did not have much experience with.

#### Attempting to live report on Linux phones only

Now, I did not just sit there. I also took photos using a pre-release build of Millipixels and posted (the first of them) to Mastodon. You may think: That's easy, why didn't you go through with it?

Honestly, if I had had a SIM card in my Librem 5, I might have, but I had somehow managed to not bring one of these SIM card tray removal pins, and thus I had to fight the difficulty with getting an internet connection on a conference full of nerds with multiple RF devices.[^3] Still, there were more obstactles: Pictures out of Millipixels aren't/weren't orientated correctly. 

Fortunately, [Image Roll](https://linuxphoneapps.org/apps/com.github.weclaw1.imageroll/) can help with this, but the resulting images larger than the source material (~10 MB instead of ~5-7 MB), a size that my Mastodon account, accessed by Tootle, did not like, maybe due to the aforementioned connectivity situation or due to other factors - it's IMHO super reasonable to enforce a file size limit on a service you offer for free.

Thus I had to find a way to resize the images. I had not planned for this, so I quickly searched for resize with apt and flatpak and ended up finding [Resizer](https://appcenter.elementary.io/com.github.peteruithoven.resizer/), an app made for Elementary OS, which, after scaling it to fit the screen with [Mobile Settings](https://linuxphoneapps.org/apps/org.sigxcpu.mobilesettings/). Now if only my skills with Imagemagick were better, I likely could have done all this in one command, but at least I found a way to do this. 

Now why did I stop posting these toots? Well, honestly, typing under time contraints on mobile keyboards wasn't fun; and I had to restart my Librem 5 to get the camera to work again - which is totally acceptable running a pre-release build of something that's clearly marked as a developer preview.[^4]. And since all these complications added up, they took my attention away from following the talks - so I decided to just relax and listen. A good idea.

#### Afternoon: Finding and visiting stands

After having sit through all of the talks in the dev room, I left with others to find the Linux on Mobile stand, which had been moved to building K, into a crowded spot, right next to stairs. With many people passing by, it was a good location, and I have been told that many people were interested to learn about Linux distributions for phones - some being even surprised that "this" is a thing.[^5] Also, on Saturday, the MNT Pocket Reform attracted some additional attention - I did not go hands on, but that prototype was definitely working and beautiful.  The downside of all this attention and the crowded space was that I did not feel comfortable with and made me feel slightly overwhelmed. 

So I took off for PINE64's stand in building Aw (a nice, old building), and guess what, it was less crowded there. I was hoping to meet Lukasz, whom I would only meet later, but I got to see the PineTab2 and plenty other PINE64 products, e.g. the Star64 and the (huge) QuartzPro 64 dev board. _More on that later._

I then set out to find the Ubuntu Touch stand, as it turned out that they had managed to set up a surprise stand in building H, demoing many devices (including one mystery device booting Tiano Core (and thus likely Coreboot)), and, of course, a convergence demo. Fortunately for me, it was less crowded. 

#### (Not) Leaving early

I walked around some more, and eventually figured that I should maybe just go home. Club Mate did not help anymore. I was tired. I had already eaten, but I still felt worse and worse. I was at the bus stop, when I saw that I had received a message, and thus went back to Aw to say Hi to Luaksz. After that, I figured: Let's pay building K a last visit. I bought a shirt, and went to the Linux on Mobile stand with the intention of telling people that I was leaving for today. Somehow, that did not happen. I walked around with Ollie to get food and decided to get a beer, which weirdly enabled me to stay longer. We ended up recording an episode of the postmarketOS podcast. After that, we took a bus and the metro and had dinner with postmarketOS and Mobian contributors.[^6]

### Day 2: Sunday

This was a day I had nothing planned for, or, to be more precise, where I had not planned to attend any talks. I met with Lukasz to have a coffee, and I played a bit with the PineTab 2. 

That aside, it was another day of moving between stands and talking to a few people - it was really nice to finally talk to so many people I knew from the internet.

### The PineTab2

PINE64 had exactly one PineTab 2 (8 GB RAM, 128 GB eMMC)  prototype at FOSDEM. Despite some Manjaro folk also manning the PINE64 stand, it ran a build of [danctnix](https://github.com/dreemurrs-embedded/Pine64-Arch) featuring Linux 6.2rc5 and the Plasma Desktop. Considering that the RK3566 is still relatively fresh, I found it to run quite well, needing to be rebooted every then and now. The tablet itsef and its keyboard accessory felt solid and well made. I think that it's going to be a nice device.

### Regrets and learnings

- I could have talked to more people,
- I did not manage to also get LinuxPhoneApps.org stickers, I could not draw attention to the project of mine that needs it most,
- I learned that arriving earlier on Friday and planning ahead (e.g., planning a meet-up early) is more important than not leaving on Sunday.

### Visual impressions

{{ gallery() }}

### Conclusions

All in all, it was a blast and I had a great time. I look forward to attending more events (I hope to be able to attend [Chemnitzer Linux Tage](https://chemnitzer.linux-tage.de/2023/en/) and/or [Linux App Summit](https://linuxappsummit.org/)), and will definitely try everything to attend FOSDEM 2024. 



[^1]: It's not really worth it, you get a cookie and sit farther apart from people that are just as annoying, but more entitled.

[^2]: That aside, I could have used KDE Itinerary, but opted for Phosh's ticket box plugin.

[^3]: Generally, I was on 3G a lot in Belgium, which made uploads tough, and being quite exhausted in general did not help, either. I also honestly did not plan for this at all.

[^4]: I would later find out that toggling the kill switch could have the same effect as restarting the camera app.

[^5]: I was also told by people at the UBports stand in building H that they still get to hear 'I thought Ubuntu Touch was a dead project' after all these years. (I could rant about how Linux publications (news sites and podcasts) are to blame for this, but that's a topic for another day.)

[^6]: Of course, my presence there had not been planned for, and unfortunately, there were no extra places in the restaurant. I only realized this after ordering - I am so sorry to have taken someone elses place!
